#!/usr/bin/python
import os

import gitlab
import sys
from gitlab import GitlabGetError

# ---------------- Variables setup

param_namespace = sys.argv[1]
param_project = sys.argv[2]
param_branch = sys.argv[3]
param_codeowners = sys.argv[4]

# ---------------- Logic
gitlabInstance = gitlab.Gitlab('https://gitlab.com', private_token=os.environ['R5_BOT_TOKEN'])
project = gitlabInstance.projects.get("%s/%s" % (param_namespace, param_project))

# ---------------- Get CODEOWNERS
codeowners = []
f = open(param_codeowners, "r")
codeowners_str = f.read()
codeowners_str = codeowners_str.split("#")[0].split("*")[1].split(",")
for codeowner_str in codeowners_str:
    codeowner_str = codeowner_str.strip().split("@")[1]
    if codeowner_str == "dummy":
        continue
    codeowners.insert(0, gitlabInstance.users.list(search=codeowner_str)[0])

# ---------------- Get protected branch
# get existing
pb = None
try:
    pb = project.protectedbranches.get(param_branch)
except GitlabGetError:
    pb = None

# Define new codeowners
co_param = []
for co in codeowners:
    co_param.insert(0, {"user_id": co.id})

# Delete if exists
if pb is not None:
    project.protectedbranches.delete(param_branch)

# create
p_branch = project.protectedbranches.create({
    'name': param_branch,
    'allowed_to_push': co_param,
    'allowed_to_merge': co_param,
    'allowed_to_unprotect': [{"access_level": gitlab.MAINTAINER_ACCESS}],
    'code_owner_approval_required': True
})
