#!/usr/bin/python
import os
import datetime

import gitlab
import sys

# ---------------- Variables setup
param_namespace = sys.argv[1]
param_project = sys.argv[2]
param_branch = sys.argv[3]
param_job_url = sys.argv[4]

split = param_job_url.split("/")
job_id = split[len(split) - 1]
today = datetime.date.today()

label = "auto::scripts-update"
title = ("[auto] %s could not be auto updated with latest changes from upstream/main." % param_branch)

issue_desc = ("The main branch could not be merged automatically into %s. A manual merge is required before "
              "auto-updates works again.\n"
              "\n"
              "Until, the script cannot be used as it is; you'll need to apply some "
              "changes localy first so think about sharing those with the community over here :heart:\n"
              "\n"
              "To help you can, you can access the original job outputs below."
              "\n| Date       | Job     |"
              "\n|------------|---------|"
              "\n| %s | [#%s](%s) |"

              % (param_branch, today, job_id, param_job_url))

# ---------------- Logic
gitlabInstance = gitlab.Gitlab('https://gitlab.com', private_token=os.environ['R5_BOT_TOKEN'])
project = gitlabInstance.projects.get("%s/%s" % (param_namespace, param_project))

# ---------------- Issue handling

# Get issue
issue = None
search = project.issues.list(state='opened', labels=[label], search=title)
for item in search:
    issue = item

# create new if not existing
if issue is None:
    issue = project.issues.create({'title': title, 'description': issue_desc, 'labels': [label]})

# Update if existing
else:
    issue.description = issue.description + ("\n| %s | [#%s](%s) |" % (today, job_id, param_job_url))
    issue.labels.insert(0, label)

# Save
issue.save()

# ---------------- MR handling
mr_desc = ("Resolve #%s" % issue.iid)

# Get issue
mr = None
search = project.mergerequests.list(state='opened', labels=[label], search=title)
for item in search:
    mr = item

# create new if not existing
if mr is None:
    mr = project.mergerequests.create({'source_branch': 'main',
                                       'target_branch': param_branch,
                                       'title': title,
                                       'description': mr_desc,
                                       'labels': [label]})

# Update if existing
else:
    mr.labels.insert(0, label)

# Save
mr.save()
