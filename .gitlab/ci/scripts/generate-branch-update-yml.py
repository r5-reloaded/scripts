#!/usr/bin/python
import re

import sys

param_template = sys.argv[1]
param_branchName = sys.argv[2]


f = open(param_template, "r")
content = f.read()
content = re.sub(r'SAFE_BRANCH_NAME', param_branchName, content)
print(content)
