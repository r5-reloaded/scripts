#!/usr/bin/python
import datetime
import os
import re

import gitlab
import sys

# ---------------- Variables setup
from gitlab import GitlabGetError

param_namespace = sys.argv[1]
param_project = sys.argv[2]
param_branch = sys.argv[3]
param_codeowners = sys.argv[4]

today = datetime.date.today()
short_branch = param_branch[8:]
page_slug = "scripts/%s" % short_branch

# ---------------- Logic
gitlabInstance = gitlab.Gitlab('https://gitlab.com', private_token=os.environ['R5_BOT_TOKEN'])
project = gitlabInstance.projects.get("%s/%s" % (param_namespace, param_project))

# ---------------- Get CODEOWNERS
codeowners = []
f = open(param_codeowners, "r")
codeowners_str = f.read()
codeowners_str = codeowners_str.split("#")[0].split("*")[1].strip()

# ---------------- Get doc content
f = open(".gitlab/doc/script-doc.md", "r")
doc_content = f.read()
doc_content_trimed = doc_content
if len(doc_content) > 50:
    doc_content_trimed = doc_content[:50] + "..."

# ---------------- Update wiki
page = project.wikis.get("Scripts-list")
content = page.content
final_content = ""
found = False
for line in iter(content.splitlines()):
    if short_branch in line:
        found = True
        final_content += "| [%s](/%s) | %s | %s | %s |\n" % (short_branch, page_slug, doc_content_trimed, codeowners_str, str(today))
    else:
        final_content += "%s\n" % line

if found is False:
    final_content += "| [%s](/%s) | %s | %s | %s |\n" % (short_branch, page_slug, doc_content_trimed, codeowners_str, str(today))
    
page.content = final_content
page.save()
