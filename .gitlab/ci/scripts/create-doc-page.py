#!/usr/bin/python
import datetime
import os
import re

import gitlab
import sys

# ---------------- Variables setup
from gitlab import GitlabGetError

param_namespace = sys.argv[1]
param_project = sys.argv[2]
param_branch = sys.argv[3]
param_codeowners = sys.argv[4]

today = datetime.date.today()
short_branch = param_branch[8:]
page_slug = "scripts/%s" % short_branch

# ---------------- Logic
gitlabInstance = gitlab.Gitlab('https://gitlab.com', private_token=os.environ['R5_BOT_TOKEN'])
project = gitlabInstance.projects.get("%s/%s" % (param_namespace, param_project))

# ---------------- Get CODEOWNERS
codeowners = []
f = open(param_codeowners, "r")
codeowners_str = f.read()
codeowners_str = codeowners_str.split("#")[0].split("*")[1].strip()

# ---------------- Get doc content
f = open(".gitlab/doc/script-doc.md", "r")
doc_content = f.read()

# ---------------- Get header content
f = open(".gitlab/doc/template.md", "r")
template = f.read()
template = re.sub(r'_SCRIPT_NAME_', short_branch, template)
template = re.sub(r'_BRANCH_URL_', "https://gitlab.com/r5-reloaded/scripts/-/tree/%s" % param_branch, template)
template = re.sub(r'_AUTHOR_', codeowners_str, template)
template = re.sub(r'_UPDATE_TS_', str(today), template)
template = re.sub(r'_DESC_', doc_content, template)

# ---------------- Update wiki
# Get existing page
page = None
try:
    page = project.wikis.get(page_slug)
except GitlabGetError:
    page = None

# Delete if exists
if page is not None:
    page.delete()

# Create
project.wikis.create({'title': page_slug, 'content': template})
